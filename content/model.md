# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en snöröjning och varje kolumn motsvarar en egenskap för den snöröjningen. 9 attribut är definierade, där de första 6 är obligatoriska.

Observera att en rapporterad åtgärd inte behöver betyda att t.ex. en väg är helt körbar. Rapporterad data betyder inte att vägen är kvalitetssäkrad.

<div class="note" title="1">

Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige. 

</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som hanterade snöröjningen. |
|[**supplier_key** ](#supplier_key) |0..1|text |**Obligatoriskt** - Ange sträng som identifierar entreprenörens systemleverantör. |
|[**timestamp** ](#timestamp)|0..1|dateTime|**Obligatoriskt** - Ange tidpunkt för när åtgärden utfördes. Varje positionsangivelse ska ha en tidpunkt. Tidpunkt anges enligt ISO 8601 med TZD. |
|[**longitude** ](#longitude)|0..1|decimal|**Obligatoriskt** - Ange information om position för fordonet enligt WGS84. |
|[**latitude** ](#latitude)|0..1|decimal|**Obligatoriskt** - Ange information om position för fordonet enligt WGS84. |
|[**measure_type**](#measure_type)|1|IN &vert; GC &vert; PL &vert; MG &vert; CG &vert; DB &vert; IR &vert; SW &vert; HM &vert; HC &vert; GP &vert; RC &vert; SS &vert; SC &vert; EC &vert; OT|**Obligatoriskt** - Ange åtgärdstyp som skedde vid tidpunkten. |
|[material](#material)|0..1|text|Anger det material som används vid aktuell åtgärd. |
|[dosage](#dosage)|0..1|text|Anger mängden material per yta, gram/kvadratmeter. |
|[spread_width](#spread_width)|0..1|text|Anger spridningsbredden på material. |

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, d.v.s. inga inledande eller eftersläpande tecken tillåts.

### **heltal**

Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**

Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

</div>

## Förtydligande av attribut

### **source**

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### **supplier_key**

Detta skulle i teorin kunna vara vilken systemleverantör som helst men enligt marknadsläget i skrivande stund är det BMSystem, Zeekit, ENEA eller annan "supplier key" som anges. Tagare av data, t.ex. kommunen och Trafikverket vill veta från vilken källa som rapportmeddelandet kommer ifrån.

### **timestamp**

Ange tidpunkten för när åtgärden utfördes, varje positionsangivelse ska ha en tidpunkt, detta för att kunna skapa en sträckning över det snöröjda/halkbekämpade området. 

### **longitude**

Anger positionen för fordonet för det inrapporterade tidpunkten enligt Trafikverkets specifikation för MIP-protokollet. Koordinater anges enligt WGS84 (formaterat i grader och med decimaler).

### **latitude**

Anger positionen för fordonet för det inrapporterade tidpunkten enligt Trafikverkets specifikation för MIP-protokollet. Koordinater anges enligt WGS84 (formaterat i grader och med decimaler).

### **measure_type**

Det finns många olika åtgärdstyper enligt Trafikverkets specifikation för hur systemleverantörer idag ska dela data enligt MIP:s protokollet. Detta finns således idag för att rapportera olika typer av insatser på vägarna. Anges på engelska som följande: inspektion, inspektion GC, halkbekämpning mekanisk, halkbekämpning kemisk, snöröjning, dammbindning, isrivning, sopning, slåtter, slåtterkomplettering, grushyvling, snöstör, kantstolpstvätt, sprickor och hål, stödremsa samt övrigt. Anges som följande:

IN (Inspection, Inspektion)<br>
GC (InspectionGC, Inspektion GC)<br>
PL (Plowing, Snöröjning)<br>
MG (MechanicalGritting, Halkbekämpning mekanisk)<br>
CG (ChemicalGritting, Halkbekämpning kemisk)<br>
DB (DustBinding, Dammbindning)<br>
IR (IceRipping, Isrivning)<br>
SW (Sweeping, Sopning)<br>
HM (HayMaking, Slåtter)<br>
HC (HayMakingCompletion, Slåtterkomplettering)<br>
GP (GravelPlaning, Grushyvling)<br>
RC (RoadCracks, Sprickor och hål)<br>
SS (Supportstrip, Stödremsa)<br>
SC (SnowCaber, Snöstör)<br>
EC (EdgePostCleaning, Kantstolpstvätt)<br>
OT (Other, Övrigt)<br>

### **material**

Anger det material som används vid aktuell åtgärd. Det kan exempelvis vara salt, grus, kemiska medel etc.

### **dosage**

Anger mängden material per yta i gram/kvadratmeter. Detta kallas för giva och är intressant för myndigheternas uppföljning och efterfrågas av forskare på VTI.

### **spread_width**

Anger spridningsbredden på material som är inställt på snöröjnings- eller halkbekämpningsfordonet. Detta är intressant för myndigheternas uppföljning och efterfrågas av forskare på VTI.