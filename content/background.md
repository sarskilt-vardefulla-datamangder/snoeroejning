**Bakgrund**

Syftet med denna specifikation är att beskriva information om en snöröjningsaktivitet, på ett enhetligt och standardiserat vis. 

Specifikationen syftar till att ge kommuner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver entiteter, d.v.s. aktiviteter om snöröjning på ett enkelt sätt. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Nationell Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>
