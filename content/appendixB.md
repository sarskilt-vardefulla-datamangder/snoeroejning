# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "source":  "2120002338",
    "supplier_key": "ZEEKIT",
    "timestamp": "20181001132023",
    "longitude":  "60.6761",
    "latitude":  "17.1531",
    "measure_type":  "IN",
    "material":  "",      
    "dosage":  "",
    "spread_width":  "",
}
```